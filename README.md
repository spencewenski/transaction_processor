# Transaction Processor
<a href="https://gitlab.com/spencewenski/transaction_processor/commits/master"><img alt="pipeline status" src="https://gitlab.com/spencewenski/transaction_processor/badges/master/pipeline.svg" /></a>

This a Rust CLI program to transform transaction records of various financial
accounts to different formats. This is intended to help with importing bank and
credit card statements into a spreadsheet or other budget system. The program
is fairly flexible; it can support many different statement formats just by
modifying the configuration file provided at run time.


## Code Documentation
Code documentation can be found [here](https://spencewenski.gitlab.io/transaction_processor).
